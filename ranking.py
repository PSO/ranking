"""
A module for establishing a *ranking* among several players,
according to a set of multiplayer *confrontations*.

The outcome of a confrontation is a mapping from players (strings) to their score (floating value).

The system establishes a *rating* that is also a mapping from players to floating values.
This rating reflects the strength difference between players,
even for those who were never directly opposed.

The position in the rating gives the ranking.

Freely inspired from the Elo rating system and from https://pypi.org/project/elo/
"""

__author__ = "Pascal Sotin"
__license__ = "GNU General Public License v3.0"

import itertools
from typing import Tuple, Sequence, Iterable, Optional, List


class Scores(object):
    """
    Representation of a mapping between players and their score.

    >>> Scores(A=3, B=4.5)
    Scores(B=4.50, A=3.00)

    Can be used both to reflect a real confrontation or a synthetic rating.
    """

    def __init__(self, **scores: float):
        self.scores = scores

    def __repr__(self):
        scores = ', '.join(f'{player}={score:.2f}' for player, score in self)
        return f'{self.__class__.__name__}({scores})'

    def __copy__(self):
        return self.__class__(**self.scores)

    def __iter__(self):
        """
        Players and scores, from the highest to the lowest.
        """
        return iter(sorted(self.scores.items(), key=lambda pair: (-pair[1], pair[0])))

    def __getitem__(self, item):
        return self.scores[item]

    def players(self) -> Iterable[str]:
        """
        Competing players.
        """
        return self.scores.keys()

    @classmethod
    def averages(cls, confrontations: Sequence['Scores']) -> 'Scores':
        """
        Compute the average score level, per player, on a set of confrontations.
        """
        acc = {}
        for c in confrontations:
            for player, score in c:
                acc_sc, acc_n = acc.setdefault(player, (0, 0))
                acc[player] = acc_sc + score, acc_n + 1
        scores = {player: sc / n for player, (sc, n) in acc.items()}
        return cls(**scores)

    def ranking(self, tie_zone: float = 0, rev=False) -> List[Tuple[int, List[str]]]:
        """
        :param tie_zone: two competitors in this zone are considered ex-aquo.
        :param rev: if True, consider that the smallest score is the best score.
        :return: a list of pairs (position, players)

        >>> Scores(A=1, B=5, C=3, D=3).ranking()
        [(1, ['B']), (2, ['C', 'D']), (4, ['A'])]
        >>> Scores(A=1, B=5, C=3, D=3).ranking(rev=True)
        [(1, ['A']), (2, ['D', 'C']), (4, ['B'])]
        """
        ranking = []
        if not rev:
            ordered = iter(self)
        else:
            ordered = reversed(list(self))
        ref = None
        for i, (p, s) in enumerate(ordered, start=1):
            if ref is not None and abs(ref - s) <= tie_zone:
                ranking[-1][1].append(p)
            else:
                ranking.append((i, [p]))
                ref = s
        return ranking

    def order_discrepancy_1vs1(self, confrontation: 'Scores', player_a: str, player_b: str) -> float:
        delta_ref = self[player_a] - self[player_b]
        delta_score = confrontation[player_a] - confrontation[player_b]
        return delta_ref - delta_score

    def order_discrepancy(self, confrontation: 'Scores') -> float:
        """
        Tell how much the score ordering is conflicting with the reference score.

        >>> # Unsurprising outcome for the confrontation
        >>> Scores(A=3, B=2).order_discrepancy(confrontation=Scores(A=2, B=1))
        0.0
        >>> # Quite surprising outcome for the confrontation!
        >>> Scores(A=3, B=2).order_discrepancy(confrontation=Scores(A=0, B=3))
        32.0
        """
        res = 0.
        for a, b in itertools.permutations(confrontation.players(), 2):
            od = self.order_discrepancy_1vs1(confrontation, a, b)
            res += od ** 2
        return res


class RatingSystem(object):
    """
    Objects able to establish ratings.
    Its attributes are the parameters of the system.

    One typically need only one object of that kind.
    """

    def __init__(self, k_factor: float = 0.2, trace: int = 0):
        """
        :param k_factor: Volatility (somewhere below 0.5 can be nice).
        :param trace: Printing level (0 = mute)
        """
        self.k_factor = k_factor
        self.trace = trace

    def gain_1vs1(self, current_ratings: Scores,
                  player: str, player_score: float,
                  opponent: str, opponent_score: float):
        """
        Compute the gain or loss provided by the result of a one vs one confrontation
        with respect to the players rankings.

        What is won by one is lost by the other.

        >>> # players performing as expected
        >>> RatingSystem(0.1).gain_1vs1(Scores(A=10, B=5), 'A', 10, 'B', 5)
        0.0
        >>> # difference between players is as expected
        >>> RatingSystem(0.1).gain_1vs1(Scores(A=10, B=5), 'A', 5, 'B', 0)
        0.0
        >>> # slight gain for player A
        >>> RatingSystem(0.1).gain_1vs1(Scores(A=10, B=5), 'A', 12, 'B', 5)
        0.2
        >>> # loss for player A w.r.t. its raking
        >>> RatingSystem(0.1).gain_1vs1(Scores(A=10, B=5), 'A', 5, 'B', 5)
        -0.5
        """
        expected_difference = current_ratings[player] - current_ratings[opponent]
        obtained_difference = player_score - opponent_score
        inaccuracy = obtained_difference - expected_difference
        return self.k_factor * inaccuracy

    def update(self, current_ratings: Scores, confrontations: Sequence[Scores]) -> Tuple[Scores, float]:
        """
        Give the new rating taking into account a set of confrontations.
        Also provide the update level, that is, a positive value reflecting the maximal change.

        >>> # slight gain for player A
        >>> RatingSystem(0.1).update(Scores(A=10, B=5), [Scores(A=12, B=5)])
        (Scores(A=10.20, B=4.80), 0.2)
        >>> # loss for player A w.r.t. its raking
        >>> RatingSystem(0.1).update(Scores(A=10, B=5), [Scores(A=5, B=5)])
        (Scores(A=9.50, B=5.50), 0.5)
        """
        gains = {player: 0 for player in current_ratings.players()}
        for i, c in enumerate(confrontations):
            for a, b in itertools.permutations(c.players(), 2):
                if self.trace >= 2:
                    print("  Pool:", i, "Playing match:", a, "vs", b)
                gains[a] += self.gain_1vs1(current_ratings, a, c[a], b, c[b])
        update_level = max(abs(u) for u in gains.values())
        ratings = {player: r + gains[player] for player, r in current_ratings}
        return Scores(**ratings), update_level

    THRESHOLD = 1e-6
    LIMIT = 1000

    def stabilize(self, initial_ratings: Scores, confrontations: Sequence[Scores],
                  threshold: float = THRESHOLD,
                  limit: Optional[int] = LIMIT) -> Scores:
        """
        Updates repeatedly a rating with the same set of confrontations, until it stabilizes.

        :param initial_ratings: Initial ratings. Must rate all participants.
        :param confrontations: Set of confrontations (assumed to be repeated with same outcome).
        :param threshold: The algorithm stops when the update level goes below the threshold.
        :param limit: The algorithm fails when the number of iterations reach the limit.
        :return: A stabilized rating (or fail).
        """
        if self.trace >= 1:
            print('Initial rating:', initial_ratings)
        current_ratings = initial_ratings
        iteration_number = 0
        while limit is None or iteration_number < limit:
            iteration_number += 1
            current_ratings, update_level = self.update(current_ratings, confrontations)
            if self.trace >= 1:
                print('Iteration:', iteration_number, current_ratings, 'Update level:', update_level)
            if update_level <= threshold:
                return current_ratings
        params = f'K={self.k_factor}, threshold={threshold}'
        raise Exception(f'no stabilization after {limit} iterations ({params})')

    def from_confrontations(self, confrontations: Sequence[Scores],
                            threshold: float = THRESHOLD,
                            limit: Optional[int] = LIMIT) -> Scores:
        """
        Compute a rating from a set of confrontations.

        >>> RatingSystem().from_confrontations([Scores(A=2, B=1), Scores(B=2, C=1)])
        Scores(A=2.50, B=1.50, C=0.50)
        >>> RatingSystem().from_confrontations([Scores(A=5, B=4), Scores(A=3, B=5)])
        Scores(B=4.50, A=4.00)
        >>> RatingSystem().from_confrontations([Scores(A=2, B=1), Scores(A=2, C=1)])
        Scores(A=2.00, B=1.00, C=1.00)
        """
        initial_ratings = Scores.averages(confrontations)
        return self.stabilize(initial_ratings, confrontations, threshold, limit)


def main():
    confrontations = (
        Scores(A=5, B=4, E=3),
        Scores(B=5, C=4),
        Scores(C=5, D=4),
        Scores(D=5, E=4),
    )
    RatingSystem(trace=True).from_confrontations(confrontations)


if __name__ == '__main__':
    main()
