# Ranking

Establishing a ranking among players from a set of multiplayer games.

## Usage
This project was used to sum up the grades of the students from IUT Blagnac, France, in a programmation contest, with peer review.

## Authors
Pascal Sotin.

## License
GNU GPL v3 (see LICENCE).
