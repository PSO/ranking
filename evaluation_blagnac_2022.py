"""
Module for ranking a very specific competition.
Influenced by the categories of that competition and the partial results format.
"""

import re
from sys import stderr
from typing import Sequence, Tuple

from openpyxl import load_workbook

from ranking import RatingSystem, Scores
from workbook_dump import Dumper


Judge = str  # Judge
Category = str  # Category
Prog = str  # Competing program


class Loader(object):

    def __init__(self):
        self.structure = (
            ("efficacité", 4),
            ("simplicité", 5),
            ("sobriété", 3))

    @staticmethod
    def parse_competition(cells):
        i = 0
        ranking = {}
        while i + 1 < len(cells):
            prog_num = None
            if cells[i] is not None and cells[i + 1] is not None:
                if isinstance(cells[i], str):
                    num_txt = re.search(r'\d+', cells[i])
                    if num_txt:
                        prog_num = int(num_txt.group())
                else:
                    prog_num = int(cells[i])
                if prog_num is not None:
                    prog = f'P{prog_num}'
                    score = cells[i + 1]
                    ranking[prog] = score
                else:
                    print(f"Skipping '{cells[i]}'", file=stderr)
            i += 2
        return ranking

    def parse_scores(self, cells):
        res = {}
        start = 0
        for competition, n in self.structure:
            end = start + 2 * n
            res[competition] = Loader.parse_competition(cells[start:end])
            start = end
        return res

    def process(self, input_file: str):
        workbook = load_workbook(input_file, read_only=True, data_only=True)
        main_sheet = workbook[workbook.sheetnames[0]]
        header, *data = main_sheet.iter_rows(values_only=True)
        i_mail = header.index('Email Address')
        result = []
        for r in data:
            judge = r[i_mail]
            item = judge, self.parse_scores(r[i_mail + 1:])
            result.append(item)
        return result


class Data(object):
    """
    Represents the complete input data.
    """

    def __init__(self, data: list[Tuple[Judge, dict[Category, dict[Prog, float]]]]):
        self.data = data

    def __repr__(self):
        return repr(self.data)

    def __iter__(self):
        return iter(self.data)

    def __len__(self):
        return len(self.data)

    @classmethod
    def load(cls, input_file: str) -> 'Data':
        return cls(Loader().process(input_file))

    def competition(self, name: str) -> Sequence[Scores]:
        res = []
        for judge, judgments in self.data:
            res.append(Scores(**judgments[name]))
        return res


def process_competition(data, name: str, output: Dumper):
    print("#", name, "#")
    pool = data.competition(name)
    print("Competitions:", pool)
    average = Scores.averages(pool)
    print("Average:", average)
    rating = RatingSystem(k_factor=0.05).stabilize(average, pool, limit=10000, threshold=1e-03)
    print("Rating:", rating)
    output.append(f"scores {name}",
                  {k: [(average[k], rating[k])] for k in rating.players()},
                  headers=("Programme", "Moyenne", "Rating"))
    ranking = rating.ranking(tie_zone=0.1)
    print("Ranking:", ranking)
    output.append(f"classement {name}",
                  [(k, *v) for k, v in ranking],
                  headers=("Place", "Programme(s)..."))
    return rating


def output_programs(data, output: Dumper):
    nb_judges = len(data)
    dict_data = {judge: judgments for judge, judgments in data}
    if nb_judges != len(dict_data):
        print("Warning: email are not unique!", file=stderr)
    print("Data as dict:", dict_data)
    output.append('données', dict_data,
                  headers=("Juge", "Catégorie", "Programme", "Note"))
    progs = {}
    for judgments in dict_data.values():
        for category, scores in judgments.items():
            for pid in scores.keys():
                prog_descr = progs.setdefault(pid, {})
                prog_descr[category] = prog_descr.setdefault(category, 0) + 1

    output.append('compétiteurs',
                  sorted([(p, c, n) for p, r in progs.items() for c, n in r.items()]),
                  headers=("Programme", "Catégorie", "Occurences"))


def output_discrepancy(data: Data, ratings: dict[str,Scores], output: Dumper) -> None:
    disc = {}
    for judge, judgments in data:
        disc[judge] = {}
        for category, scores in judgments.items():
            scores = Scores(**scores)
            disc[judge][category] = ratings[category].order_discrepancy(scores)
    output.append("Anomalies", disc,
                  headers=("Juge", "Catégorie", "Score d'anomalie"))


def main():
    data = Data.load('2022/Concours de code (Responses).xlsx')
    output = Dumper()
    output_programs(data, output)
    ratings = {}
    for category in ("efficacité", "simplicité", "sobriété"):
        ratings[category] = process_competition(data, category, output)
    output_discrepancy(data, ratings, output)
    output.write_to("2022/resultats.xlsx")


if __name__ == '__main__':
    main()
