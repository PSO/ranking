"""
Utility module for dumping sheets and data in xlsx.
Rely itself on openpyxl.
"""

from typing import Sequence, Union, Dict, Iterable

from openpyxl import Workbook
from openpyxl.styles import Font


class Dumper(object):
    """
    A workbook intended to be write-only.
    Its `append` method create and populate a new worksheet.
    """

    def __init__(self):
        self.wb = Workbook()
        self.wb.remove(self.wb.active)  # Empty workbook

    def append(self, title: str, data: Union[Sequence, Dict], headers: Sequence = None):
        ws = self.wb.create_sheet(title)
        if headers is not None:
            ft = Font(bold=True)
            for c, h in enumerate(headers, start=1):
                ws.cell(row=1, column=c, value=h).font = ft
            next_row = 2
        else:
            next_row = 1
        self.Worksheet(ws).write_vertically(next_row, 1, data)

    def write_to(self, filename) -> None:
        """
        Dumps the current workbook into a file.
        """
        self.wb.save(filename)

    class Worksheet(object):

        def __init__(self, ws):
            self.ws = ws

        def write_cell(self, row, col, data) -> None:
            if not (data is None or isinstance(data, (int, float))):
                data = str(data)
            self.ws.cell(row, col, data)

        @staticmethod
        def decompose(data):
            if isinstance(data, str) or isinstance(data, bytes):
                return None  # False iterable
            elif isinstance(data, Iterable):
                try:
                    *keys, last = data
                    if keys:
                        return keys, last
                except ValueError:
                    pass
            return None

        def write_horizontally(self, cur_row, cur_col, data) -> int:
            """
            :param cur_row: Where to start (row)
            :param cur_col: Where to start (column)
            :param data: Data to write
            :return: the next row
            """
            if isinstance(data, Dict):
                for k, v in data.items():
                    next_row = self.write_vertically(cur_row, cur_col + 1, v)
                    for row in range(cur_row, next_row):
                        self.write_cell(row, cur_col, k)
                    cur_row = next_row
                return cur_row
            else:
                decom = self.decompose(data)
                if decom is None:
                    self.write_cell(cur_row, cur_col, data)
                    return cur_row + 1
                else:
                    keys, last = decom
                    next_row = self.write_vertically(cur_row, cur_col + len(keys), last)
                    for row in range(cur_row, next_row):
                        for col, k in enumerate(keys, start=cur_col):
                            self.write_cell(row, col, k)
                    return next_row

        def write_vertically(self, cur_row, cur_col, data) -> int:
            if (not isinstance(data, str) and not isinstance(data, bytes)
                    and isinstance(data, Sequence)):
                for rows in data:
                    cur_row = self.write_horizontally(cur_row, cur_col, rows)
                return cur_row
            else:
                return self.write_horizontally(cur_row, cur_col, data)


def main():
    d = Dumper()
    d.append("F1", [1, 2, 3], ["N"])
    d.append("F2", [('A', 1), ('B', 2)], ["Letter", "Number"])
    d.append("F2bis", {'A': 1, 'B': 2}, ["Letter", "Number"])
    d.append("F3", {'A': [1, 2], 'B': [3, 4]}, ["Letter", "Number"])
    d.append("F3bis", [{'A': [1, 2]}, {'B': [3, 4]}], ["Letter", "Number"])
    d.append("F3ter", [('A', [1, 2]), ('B', [3, 4])], ["Letter", "Number"])
    d.append("F4", [(3, 'P92'), (4, 'P113', 'P136'), (6, 'P57')])
    d.append("F5", {'A': {'c': [[1, 2], [3, 4]], 'd': [[[5]]]}, 'B': None}, ["Letter", "sub", "Number"])
    d.write_to("dumper_demo.xlsx")


if __name__ == '__main__':
    main()
